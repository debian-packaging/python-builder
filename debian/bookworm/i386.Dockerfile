# Copyright 2020-2025 IQRF Tech s.r.o.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM iqrftech/debian-base-builder:debian-bookworm-i386

LABEL maintainer="Roman Ondráček <roman.ondracek@iqrf.com>"

ENV DEBIAN_FRONTEND="noninteractive"

RUN apt-get update \
  && apt-get install --no-install-recommends -y \
     dh-python python3-all python3-coverage python3-jsonschema \
     python3-paho-mqtt python3-nose python3-pycodestyle python3-pydocstyle \
     python3-pytest python3-pytest-cov python3-requests python3-responses \
     python3-systemd pylint twine \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*
